The main goals of this app are:

 1. Make streaming easy, accessible, and stunning
 2. Provide a curated, premium experience

Liveblast must be useful from the get go. There must be zero configuration, or
as close as possible to that. People should spend all their time on content and
execution, and zero time worrying about stream details, like scene layouts or
effects. Liveblast must provide dazzling scene layouts and effects by default.

Liveblast must reduce the barrier for streaming on Linux. It must showcase the
capabilities of the Linux platform, and improve the platform if it doesn't
provide the necessary features. It is strictly distributed through Flatpak, on
Flathub, operates under a strict sandboxed, and relies exclusively on portals.

Liveblast must be light on resources, as the contents of the stream itself might
be heavy - think of gaming or development.

Liveblast must be tightly integrated with streaming platforms. It must provide
enough features for streamers to stream their content, monitor stream quality,
and interact with their viewers - either through chat or stream overlays.

### Non-Goals

**Video editing**: Liveblast is focused on streaming content, not editing it.
There is a plethora of excellent video editors for Linux!

**Scene designer**: the set of scene layouts that Liveblast ships is curated,
so as to offload this burden from users. We might support scene packs of other
forms of content sharing in the future, but in principle, scenes layouts are
pre-defined and curated.

## Constraints

 * Light on resources
   * Unconditionally rely on hardware acceleration (OpenGL, VA-API, etc)
   * Optimized video compositing pipelines
 * Sandboxed
   * No workarounds or sandbox holes
   * We must make impossible features possible
   * Flathub-only

