// transmitter.rs
//
// Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

// # Transmitter Pipeline
//
// appsrc -- queue -- h264parse -- flvmux -- rtmp2sink
//                               /
//  appsrc -- queue -- avenc_aac

use gst::prelude::*;
use gtk::glib;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use std::cell::RefCell;

use crate::engine::pluggable::*;
use crate::engine::subpipeline::*;
use crate::engine::StreamingService;
use crate::engine::{DEFAULT_SERVICES, SETTINGS};

pub struct TransmitterSettings {
    pub service: Option<StreamingService>,
    pub stream_key: String,
}

mod imp {
    use super::*;

    pub struct Transmitter {
        pub(super) audio_appsrc: gst_app::AppSrc,
        pub(super) video_appsrc: gst_app::AppSrc,
        pub(super) settings: RefCell<TransmitterSettings>,
        pub(super) subpipeline: SubPipeline,
        pub(super) producers: RefCell<ContentConsumptionList>,
        pub(super) rtmp2sink: gst::Element,
    }

    fn service_from_settings() -> Option<StreamingService> {
        let id = SETTINGS.string("service");
        DEFAULT_SERVICES.iter().find(|s| s.id == id).cloned()
    }

    fn stream_key_from_settings() -> String {
        String::from(SETTINGS.string("stream-key"))
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Transmitter {
        const NAME: &'static str = "LbTransmitter";
        type Type = super::Transmitter;

        fn new() -> Self {
            let settings = TransmitterSettings {
                service: service_from_settings(),
                stream_key: stream_key_from_settings(),
            };

            Self {
                audio_appsrc: gst_app::AppSrc::builder()
                    .is_live(true)
                    .name("audio_appsrc")
                    .build(),
                video_appsrc: gst_app::AppSrc::builder()
                    .is_live(true)
                    .name("video_appsrc")
                    .build(),
                rtmp2sink: gst::ElementFactory::make("rtmp2sink").build().unwrap(),
                producers: RefCell::new(ContentConsumptionList::default()),
                settings: RefCell::new(settings),
                subpipeline: SubPipeline::new("Transmitter"),
            }
        }
    }

    impl ObjectImpl for Transmitter {
        fn constructed(&self) {
            self.parent_constructed();

            self.obj().update_location();

            let queue = gst::ElementFactory::make("queue").build().unwrap();
            let h264parse = gst::ElementFactory::make("h264parse").build().unwrap();
            let queue2 = gst::ElementFactory::make("queue").build().unwrap();
            let fdkaacenc = gst::ElementFactory::make("fdkaacenc")
                .property("bitrate", 160000)
                .build()
                .unwrap();
            let flvmux = gst::ElementFactory::make("flvmux")
                .property_from_str("start-time-selection", "first")
                .property("streamable", true)
                .build()
                .unwrap();

            self.obj()
                .pipeline()
                .add_many(&[
                    self.video_appsrc.upcast_ref(),
                    &queue,
                    self.audio_appsrc.upcast_ref(),
                    &queue2,
                    &fdkaacenc,
                    &h264parse,
                    &flvmux,
                    &self.rtmp2sink,
                ])
                .unwrap();

            gst::Element::link_many(&[
                self.video_appsrc.upcast_ref(),
                &queue,
                &h264parse,
                &flvmux,
                &self.rtmp2sink,
            ])
            .unwrap();
            gst::Element::link_many(&[
                self.audio_appsrc.upcast_ref(),
                &queue2,
                &fdkaacenc,
                &flvmux,
            ])
            .unwrap();
        }
    }
}

impl HasSubPipeline for Transmitter {
    fn subpipeline(&self) -> &SubPipeline {
        &self.imp().subpipeline
    }
}

glib::wrapper! {
    pub struct Transmitter(ObjectSubclass<imp::Transmitter>);
}

impl ContentConsumer for Transmitter {
    fn consume_content_from(&self, producer: impl ContentProducer) {
        if let Some(video_producer) = producer.stream_producer(ContentType::ENCODED_VIDEO) {
            let link = video_producer
                .add_consumer(&self.imp().video_appsrc)
                .unwrap();
            self.imp().producers.borrow_mut().push(
                producer.id(),
                ContentType::ENCODED_VIDEO,
                video_producer,
                link,
                None,
                &[],
            );
        }

        if let Some(audio_producer) = producer.stream_producer(ContentType::AUDIO) {
            let link = audio_producer
                .add_consumer(&self.imp().audio_appsrc)
                .unwrap();
            self.imp().producers.borrow_mut().push(
                producer.id(),
                ContentType::AUDIO,
                audio_producer,
                link,
                None,
                &[],
            );
        }
    }
}

impl Default for Transmitter {
    fn default() -> Self {
        glib::Object::new()
    }
}

impl Transmitter {
    pub fn apply_settings(&self, settings: TransmitterSettings) {
        self.imp().settings.replace(settings);

        self.update_location();
        self.save_settings();
    }

    fn update_location(&self) {
        let settings = self.imp().settings.borrow();

        if let Some(service) = &settings.service {
            let location = service.format_location_url(&settings.stream_key);
            self.imp().rtmp2sink.set_property("location", location);
        };
    }

    fn save_settings(&self) {
        let settings = self.imp().settings.borrow();

        SETTINGS
            .set_string(
                "service",
                settings.service.as_ref().map(|s| s.id).unwrap_or(""),
            )
            .expect("Failed to save service");

        // TODO: use the secrets portal to store this
        SETTINGS
            .set_string("stream-key", &settings.stream_key)
            .expect("Failed to save stream key");
    }
}
