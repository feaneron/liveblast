// engine.rs
//
// Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

use gtk::glib;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use log::debug;

use crate::engine::elements::*;
use crate::engine::*;

mod imp {
    use super::*;
    use std::cell::OnceCell;

    #[derive(Debug, Default, glib::Properties)]
    #[properties(wrapper_type = super::Engine)]
    pub struct Engine {
        pub(super) desktop_audio: OnceCell<DesktopAudioElement>,
        pub(super) camera: OnceCell<CameraElement>,
        pub(super) microphone: OnceCell<MicrophoneElement>,
        #[property(get)]
        pub(super) pipeline: OnceCell<Pipeline>,
        pub(super) screencast: OnceCell<ScreencastElement>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Engine {
        const NAME: &'static str = "LbEngine";
        type Type = super::Engine;
    }

    impl ObjectImpl for Engine {
        fn constructed(&self) {
            self.parent_constructed();

            debug!("Initializing GStreamer");
            gst::init().ok();
            gstgtk4::plugin_register_static().expect("Failed to initalize gstgtk4");

            let pipeline = Pipeline::default();

            let microphone_element = MicrophoneElement::default();
            pipeline.add_stream_element(microphone_element.clone());
            self.microphone.set(microphone_element).unwrap();

            let desktop_audio_element = DesktopAudioElement::default();
            pipeline.add_stream_element(desktop_audio_element.clone());
            self.desktop_audio.set(desktop_audio_element).unwrap();

            let screencast_element = ScreencastElement::default();
            pipeline.add_stream_element(screencast_element.clone());
            self.screencast.set(screencast_element).unwrap();

            let camera_element = CameraElement::default();
            pipeline.add_stream_element(camera_element.clone());
            self.camera.set(camera_element).unwrap();

            self.pipeline.set(pipeline).unwrap();
        }

        fn properties() -> &'static [glib::ParamSpec] {
            Self::derived_properties()
        }

        fn property(&self, id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            self.derived_property(id, pspec)
        }
    }
}

glib::wrapper! {
    pub struct Engine(ObjectSubclass<imp::Engine>);
}

impl Default for Engine {
    fn default() -> Self {
        glib::Object::new()
    }
}

impl Engine {
    pub fn camera(&self) -> CameraElement {
        self.imp().camera.get().unwrap().clone()
    }

    pub fn desktop_audio(&self) -> DesktopAudioElement {
        self.imp().desktop_audio.get().unwrap().clone()
    }

    pub fn microphone(&self) -> MicrophoneElement {
        self.imp().microphone.get().unwrap().clone()
    }

    pub fn screencast(&self) -> ScreencastElement {
        self.imp().screencast.get().unwrap().clone()
    }

    pub fn start_stream_elements(&self) {
        let self_ = self.to_owned();

        glib::MainContext::default().spawn_local(async move {
            self_.camera().load().await.unwrap();
            self_.microphone().load().await.unwrap();
            self_.desktop_audio().load().await.unwrap();
            self_.screencast().load().await.unwrap();
        });
    }
}
