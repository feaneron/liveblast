// plug.rs
//
// Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

use bitflags::bitflags;

bitflags! {
    pub struct ContentType: u32 {
        const AUDIO = 0b00000001;
        const RAW_VIDEO = 0b00000010;
        const ENCODED_VIDEO = 0b00000100;
    }
}

pub trait ContentConsumer {
    fn consume_content_from(&self, producer: impl ContentProducer);
}

pub trait ContentProducer {
    fn id(&self) -> Option<&str>;
    fn stream_producer(&self, content_type: ContentType) -> Option<gst_utils::StreamProducer>;
}

pub struct ContentConsumptionLink {
    pub id: Option<String>,
    pub content_type: ContentType,
    pub stream_producer: gst_utils::StreamProducer,
    pub consumption_link: gst_utils::ConsumptionLink,
    pub pad: Option<gst::Pad>,
    pub gst_elements: Vec<gst::Element>,
}

#[derive(Default)]
pub struct ContentConsumptionList {
    producers: Vec<ContentConsumptionLink>,
}

impl ContentConsumptionList {
    pub fn push(
        &mut self,
        id: Option<&str>,
        content_type: ContentType,
        stream_producer: gst_utils::StreamProducer,
        consumption_link: gst_utils::ConsumptionLink,
        pad: Option<gst::Pad>,
        elements: &[gst::Element],
    ) {
        self.producers.push(ContentConsumptionLink {
            id: id.map(String::from),
            content_type,
            stream_producer,
            consumption_link,
            pad,
            gst_elements: elements.to_vec()
        });
    }

    pub fn iter(&self) -> std::slice::Iter<'_, ContentConsumptionLink> {
        self.producers.iter()
    }
}
