// layout_manager.rs
//
// Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

use gst::prelude::*;
use gtk::glib;
use gtk::subclass::prelude::*;

use crate::engine::*;

mod imp {
    use super::*;
    use std::cell::OnceCell;

    #[derive(Debug, Default, glib::Properties)]
    #[properties(wrapper_type = super::LayoutManager)]
    pub struct LayoutManager {
        #[property(get, set, construct_only)]
        pub(super) compositor: OnceCell<Compositor>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for LayoutManager {
        const NAME: &'static str = "LbLayoutManager";
        type Type = super::LayoutManager;
    }

    impl ObjectImpl for LayoutManager {
        fn constructed(&self) {
            self.parent_constructed();
        }

        fn properties() -> &'static [glib::ParamSpec] {
            Self::derived_properties()
        }

        fn set_property(&self, id: usize, value: &glib::Value, pspec: &glib::ParamSpec) {
            self.derived_set_property(id, value, pspec)
        }

        fn property(&self, id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            self.derived_property(id, pspec)
        }
    }
}

glib::wrapper! {
    pub struct LayoutManager(ObjectSubclass<imp::LayoutManager>);
}

impl LayoutManager {
    pub fn new(compositor: Compositor) -> Self {
        glib::Object::builder()
            .property("compositor", compositor)
            .build()
    }

    pub fn apply_layout(&self) {
        let compositor_producers = self.imp().compositor.get().unwrap().producers();

        for link in compositor_producers.iter() {
            let glvideomixer_pad = link.pad.clone().unwrap();

            // TODO: generic scene layouts
            glvideomixer_pad.set_property_from_str("sizing-policy", "keep-aspect-ratio");

            match link.id.as_deref() {
                Some("camera") => {
                    glvideomixer_pad.set_properties(&[
                        ("xpos", &((1920 * 2 / 3 - 48) as i32)),
                        ("ypos", &((1080 * 2 / 3 - 48) as i32)),
                        ("width", &(1920 / 3_i32)),
                        ("height", &(1080 / 3_i32)),
                        ("xalign", &(1_f64)),
                        ("yalign", &(1_f64)),
                    ]);
                }
                Some("screencast") => {
                    glvideomixer_pad.set_properties(&[("width", &1920), ("height", &1080)]);
                }
                _ => unimplemented!(),
            };
        }
    }
}
