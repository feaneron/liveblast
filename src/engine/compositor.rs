// compositor.rs
//
// Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

// # Main data pipeline
//
// This is a static scene with hardcoded sources, canvas size,
//
//  [ element bin ¹ ] -- glvideomixer -- tee -- queue -- appsink
//                    /|                      \
//  [ element bin ¹ ]  |                        queue -- gtk4paintablesink
//  [ element bin ¹ ]  /
//     ...
//
// ## 1. Element bins
//
// Every connected stream element is connected to the pipeline
// using a standard GstBin. This bin contains the following
// elements:
//
//  +------- Bin ------+
//  | appsrc → queue -[-]→ glvideomixer
//  +------------------+

use gst::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{gdk, glib};
use std::cell::{Ref, RefCell};

use crate::engine::pluggable::*;
use crate::engine::subpipeline::*;

mod imp {
    use super::*;

    pub struct Compositor {
        pub(super) glvideomixer: gst::Element,
        pub(super) paintable_sink: gst::Element,
        pub(super) producers: RefCell<ContentConsumptionList>,
        pub(super) stream_producer: gst_utils::StreamProducer,
        pub(super) subpipeline: SubPipeline,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Compositor {
        const NAME: &'static str = "LbCompositor";
        type Type = super::Compositor;

        fn new() -> Self {
            let appsink = gst_app::AppSink::builder()
                .async_(true)
                .name("composer-appsink")
                .build();
            let glvideomixer = gst::ElementFactory::make("glvideomixer")
                .property_from_str("start-time-selection", "first")
                .property_from_str("background", "black")
                .build()
                .unwrap();
            let stream_producer = gst_utils::StreamProducer::from(&appsink);
            let paintable_sink = gst::ElementFactory::make("gtk4paintablesink")
                .build()
                .unwrap();

            // The paintable must always "win" and handle the GL context,
            // because it specifically creates a GL context shared that is
            // shared with GTK4. We specifically don't want the pipeline to
            // ever destroy that context when it is stopped too, so we must
            // lock the state.
            paintable_sink.set_state(gst::State::Playing).unwrap();
            paintable_sink.set_locked_state(true);

            Self {
                glvideomixer,
                paintable_sink,
                producers: RefCell::new(ContentConsumptionList::default()),
                stream_producer,
                subpipeline: SubPipeline::new("Video Compositor"),
            }
        }
    }

    impl ObjectImpl for Compositor {
        fn constructed(&self) {
            self.parent_constructed();

            let obj = self.obj();
            let pipeline = obj.pipeline();
            let capsfilter = gst::ElementFactory::make("capsfilter").build().unwrap();
            let tee = gst::ElementFactory::make("tee").build().unwrap();
            let queue = gst::ElementFactory::make("queue").build().unwrap();
            let queue2 = gst::ElementFactory::make("queue").build().unwrap();

            capsfilter.set_property(
                "caps",
                gst_video::VideoCapsBuilder::new()
                    .any_features()
                    .width(1920)
                    .height(1080)
                    .framerate(gst::Fraction::new(60, 1))
                    .build(),
            );

            let appsink = self.stream_producer.appsink();

            pipeline
                .add_many(&[
                    &self.glvideomixer,
                    &capsfilter,
                    &tee,
                    &queue,
                    appsink.upcast_ref(),
                    &queue2,
                    &self.paintable_sink,
                ])
                .unwrap();

            gst::Element::link_many(&[
                &self.glvideomixer,
                &capsfilter,
                &tee,
                &queue,
                appsink.upcast_ref(),
            ])
            .unwrap();

            gst::Element::link_many(&[&tee, &queue2, &self.paintable_sink]).unwrap();

            obj.start();
        }

        fn dispose(&self) {
            self.paintable_sink.set_state(gst::State::Null).unwrap();
        }
    }
}

impl HasSubPipeline for Compositor {
    fn subpipeline(&self) -> &SubPipeline {
        &self.imp().subpipeline
    }
}

glib::wrapper! {
    pub struct Compositor(ObjectSubclass<imp::Compositor>);
}

impl ContentConsumer for Compositor {
    fn consume_content_from(&self, producer: impl ContentProducer) {
        let stream_producer = producer
            .stream_producer(ContentType::RAW_VIDEO)
            .expect("Compositor requires a raw video producer");

        self.stop();

        let appsrc = gst_app::AppSrc::builder().is_live(true).build();
        let queue = gst::ElementFactory::make("queue").build().unwrap();

        let bin = gst::Bin::with_name(producer.id().unwrap_or(""));
        bin.add_many(&[appsrc.upcast_ref(), &queue]).unwrap();
        gst::Element::link_many(&[appsrc.upcast_ref(), &queue]).unwrap();

        let ghost_pad = gst::GhostPad::builder_with_target(&queue.static_pad("src").unwrap())
            .unwrap()
            .name("src")
            .build();
        bin.add_pad(&ghost_pad).unwrap();

        self.pipeline().add(&bin).unwrap();

        let glvideomixer_pad = self
            .imp()
            .glvideomixer
            .request_pad_simple("sink_%u")
            .unwrap();
        ghost_pad.link(&glvideomixer_pad).unwrap();

        let link = stream_producer.add_consumer(&appsrc).unwrap();
        self.imp().producers.borrow_mut().push(
            producer.id(),
            ContentType::RAW_VIDEO,
            stream_producer,
            link,
            Some(glvideomixer_pad),
            &[bin.upcast::<gst::Element>()],
        );

        self.start();
    }
}

impl ContentProducer for Compositor {
    fn id(&self) -> Option<&str> {
        None
    }

    fn stream_producer(&self, content_type: ContentType) -> Option<gst_utils::StreamProducer> {
        match content_type {
            ContentType::RAW_VIDEO => Some(self.imp().stream_producer.clone()),
            _ => None,
        }
    }
}

impl Default for Compositor {
    fn default() -> Self {
        glib::Object::new()
    }
}

impl Compositor {
    pub fn get_preview(&self) -> Option<gdk::Paintable> {
        Some(
            self.imp()
                .paintable_sink
                .property::<gdk::Paintable>("paintable"),
        )
    }

    pub(super) fn producers(&self) -> Ref<'_, ContentConsumptionList> {
        self.imp().producers.borrow()
    }

    pub(super) fn share_gl_context_with(&self, content_element: impl HasSubPipeline) {
        // Make sure that the producer will always use the same GL context
        // that gtk4paintablesink created
        let pipeline = content_element.subpipeline().pipeline();

        println!("[Compositor] Sharing GL context with {}", pipeline.name());

        let context = self
            .imp()
            .subpipeline
            .gl_context()
            .expect("Compositor failed to get a GL context!");

        pipeline.set_context(&context);
    }
}
