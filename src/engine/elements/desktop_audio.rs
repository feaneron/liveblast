// desktop_audio.rs
//
// Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

use async_trait::async_trait;
use gst::prelude::*;
use pulsectl::controllers::*;
use std::sync::Arc;

use crate::engine;
use crate::engine::pluggable::*;
use crate::engine::subpipeline::*;

#[derive(Clone)]
pub struct DesktopAudioElement {
    pulsesrc: gst::Element,
    stream_producer: gst_utils::StreamProducer,
    subpipeline: Arc<SubPipeline>,
}

impl HasSubPipeline for DesktopAudioElement {
    fn subpipeline(&self) -> &SubPipeline {
        &self.subpipeline
    }
}

impl ContentProducer for DesktopAudioElement {
    fn id(&self) -> Option<&str> {
        Some("desktop-audio")
    }

    fn stream_producer(&self, content_type: ContentType) -> Option<gst_utils::StreamProducer> {
        match content_type {
            ContentType::AUDIO => Some(self.stream_producer.clone()),
            _ => None,
        }
    }
}

impl std::fmt::Debug for DesktopAudioElement {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("DesktopAudioElement")
         .field("active", &self.active())
         .field("pipeline", &self.pipeline().name())
         .finish()
    }
}

#[async_trait]
impl engine::StreamElement for DesktopAudioElement {
    fn content_type(&self) -> ContentType {
        ContentType::AUDIO
    }

    async fn load(&self) -> Result<(), ()> {
        self.add_pulsesrc();
        self.start();
        Ok(())
    }
}

#[async_trait]
impl engine::PermissionController for DesktopAudioElement {
    async fn request_permission(
        &self,
        _window_identifier: Option<ashpd::WindowIdentifier>
    ) -> Result<engine::Permission, ashpd::Error> {
        self.add_pulsesrc();
        self.start();
        Ok(engine::Permission::Granted)
    }
}

impl DesktopAudioElement {
    fn add_pulsesrc(&self) {
        let mut sink_controller = SinkController::create()
            .expect("Could not create a sink controller");
        let server_info = sink_controller.get_server_info()
            .expect("Failed to get server info");

        let default_device = if let Some(default_sink_name) = &server_info.default_sink_name {
            sink_controller
                .get_device_by_name(&default_sink_name)
                .expect("Default sink device not found")
        } else {
            sink_controller
                .get_default_device()
                .expect("No default device set")
        };

        if let Some(name) = default_device.monitor_name {
            println!("[Desktop Audio] Using {}", &name);
            self.pulsesrc.set_property("device", name);
        }

        let appsink = self.stream_producer.appsink();

        let capsfilter = gst::ElementFactory::make("capsfilter")
            .property(
                "caps",
                [
                    gst_audio::AudioCapsBuilder::new()
                        .any_features()
                        .channels(1)
                        .build(),
                ]
                .into_iter()
                .collect::<gst::Caps>(),
            )
            .build()
            .unwrap();

        self.pipeline().add_many(&[
            &self.pulsesrc,
            &capsfilter,
            appsink.upcast_ref(),
        ])
        .unwrap();

        gst::Element::link_many(&[
            &self.pulsesrc,
            &capsfilter,
            appsink.upcast_ref(),
        ])
        .unwrap();
    }
}

impl Default for DesktopAudioElement {
    fn default() -> Self {
        let appsink = gst_app::AppSink::builder()
            .async_(true)
            .name("appsink")
            .build();
        let stream_producer = gst_utils::StreamProducer::from(&appsink);
        let pulsesrc = gst::ElementFactory::make("pulsesrc").build().unwrap();
        let subpipeline = SubPipeline::new("Desktop Audio");

        Self {
            pulsesrc,
            stream_producer,
            subpipeline: Arc::new(subpipeline),
        }
    }
}
