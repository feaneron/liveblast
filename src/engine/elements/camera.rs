// camera.rs
//
// Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::os::unix::io::AsRawFd;

use async_trait::async_trait;
use gst::prelude::*;
use std::sync::Arc;

use crate::engine;
use crate::engine::pluggable::*;
use crate::engine::subpipeline::*;

#[derive(Clone)]
pub struct CameraElement {
    stream_producer: gst_utils::StreamProducer,
    subpipeline: Arc<SubPipeline>,
}

impl HasSubPipeline for CameraElement {
    fn subpipeline(&self) -> &SubPipeline {
        &self.subpipeline
    }
}

impl ContentProducer for CameraElement {
    fn id(&self) -> Option<&str> {
        Some("camera")
    }

    fn stream_producer(&self, content_type: ContentType) -> Option<gst_utils::StreamProducer> {
        match content_type {
            ContentType::RAW_VIDEO => Some(self.stream_producer.clone()),
            _ => None,
        }
    }
}

impl std::fmt::Debug for CameraElement {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("CameraElement")
         .field("active", &self.active())
         .field("pipeline", &self.pipeline().name())
         .finish()
    }
}

#[async_trait]
impl engine::PermissionController for CameraElement {
    async fn request_permission(
        &self,
        _window_identifier: Option<ashpd::WindowIdentifier>
    ) -> Result<engine::Permission, ashpd::Error> {
        let camera = ashpd::desktop::camera::Camera::new().await?;

        match camera.request_access().await {
            Ok(request) => {
                match request.response() {
                    Ok(_) => {
                        let remote_fd = camera.open_pipe_wire_remote().await?;
                        self.start_camera(remote_fd);
                        Ok(engine::Permission::Granted)
                    },
                    Err(ashpd::Error::Portal(
                        ashpd::PortalError::NotAllowed(_)
                    )) => Ok(engine::Permission::Denied),
                    // FIXME: treat cancelled as permission denied, as that's
                    // what the portal returns sometimes. Race condition?
                    Err(ashpd::Error::Response(
                        ashpd::desktop::ResponseError::Cancelled
                    )) => Ok(engine::Permission::Denied),
                    _ => Ok(engine::Permission::Unset)
                }
            },
            Err(error) => match error {
                ashpd::Error::Portal(
                    ashpd::PortalError::NotAllowed(_)
                ) => Ok(engine::Permission::Denied),
                _ => Ok(engine::Permission::Unset)
            },
        }
    }
}

#[async_trait]
impl engine::StreamElement for CameraElement {
    fn content_type(&self) -> ContentType {
        ContentType::RAW_VIDEO
    }

    async fn load(&self) -> Result<(), ()> {
        let camera = match ashpd::desktop::camera::Camera::new().await {
            Ok(camera) => camera,
            Err(_) => return Err(()),
        };

        let fd = match camera.open_pipe_wire_remote().await {
            Ok(fd) => fd,
            Err(_) => return Err(()),
        };

        self.start_camera(fd);
        Ok(())
    }
}

impl CameraElement {
    fn start_camera<F: AsRawFd>(&self, fd: F) {
        let pipewire_element = gst::ElementFactory::make("pipewiresrc")
            .property("client-name", "Liveblast")
            .property("fd", fd.as_raw_fd())
            .build()
            .unwrap();
        let capsfilter = gst::ElementFactory::make("capsfilter")
            .property(
                "caps",
                [
                    gst_video::VideoCapsBuilder::new()
                        .any_features()
                        .build(),
                    gst::Caps::builder("image/jpeg")
                        .build(),
                ]
                .into_iter()
                .collect::<gst::Caps>(),
            )
            .build()
            .unwrap();
        let decodebin3 = gst::ElementFactory::make("decodebin3")
            .property(
                "caps",
                [
                    gst_video::VideoCapsBuilder::new()
                        .any_features()
                        .build(),
                ]
                .into_iter()
                .collect::<gst::Caps>(),
            )
            .build()
            .unwrap();
        let appsink = self.stream_producer.appsink();

        self.stop();

        self.pipeline().add_many(&[
            &pipewire_element,
            &capsfilter,
            &decodebin3,
            appsink.upcast_ref(),
        ])
        .unwrap();

        gst::Element::link_many(&[
            &pipewire_element,
            &capsfilter,
            &decodebin3,
        ])
        .unwrap();

        let appsink_clone = appsink.clone();
        decodebin3.connect_pad_added(move |_, pad| {
            if pad.name().starts_with("video_") {
                pad.link(&appsink_clone.static_pad("sink").unwrap())
                    .expect("Failed to link decodebin3:video_%u pad with appsrc:sink");
            } else {
                panic!("Audio coming through the webcam pipeline!");
            }
        });

        self.start();
    }
}

impl Default for CameraElement {
    fn default() -> Self {
        let appsink = gst_app::AppSink::builder()
            .async_(true)
            .name("appsink")
            .build();
        let stream_producer = gst_utils::StreamProducer::from(&appsink);
        let subpipeline = SubPipeline::new("Camera");

        Self {
            stream_producer,
            subpipeline: Arc::new(subpipeline),
        }
    }
}
