// screencast.rs
//
// Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::os::unix::io::AsRawFd;

use ashpd::desktop::screencast::*;
use ashpd::desktop::PersistMode;
use ashpd::enumflags2::BitFlags;

use async_trait::async_trait;
use gst::prelude::*;
use gtk::prelude::*;
use std::sync::Arc;

use crate::engine;
use crate::engine::pluggable::*;
use crate::engine::subpipeline::*;
use crate::engine::SETTINGS;

#[derive(Clone)]
pub struct ScreencastElement {
    stream_producer: gst_utils::StreamProducer,
    subpipeline: Arc<SubPipeline>,
}

impl HasSubPipeline for ScreencastElement {
    fn subpipeline(&self) -> &SubPipeline {
        &self.subpipeline
    }
}

impl ContentProducer for ScreencastElement {
    fn id(&self) -> Option<&str> {
        Some("screencast")
    }

    fn stream_producer(&self, content_type: ContentType) -> Option<gst_utils::StreamProducer> {
        match content_type {
            ContentType::RAW_VIDEO => Some(self.stream_producer.clone()),
            _ => None,
        }
    }
}

impl std::fmt::Debug for ScreencastElement {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("ScreencastElement")
            .field("active", &self.active())
            .field("pipeline", &self.pipeline().name())
            .finish()
    }
}

#[async_trait]
impl engine::PermissionController for ScreencastElement {
    async fn request_permission(
        &self,
        window_identifier: Option<ashpd::WindowIdentifier>,
    ) -> Result<engine::Permission, ashpd::Error> {
        match self
            .create_screencast_session(window_identifier, false)
            .await
        {
            Ok(_) => Ok(engine::Permission::Granted),
            Err(error) => Err(error),
        }
    }
}

#[async_trait]
impl engine::StreamElement for ScreencastElement {
    fn content_type(&self) -> ContentType {
        ContentType::RAW_VIDEO
    }

    async fn load(&self) -> Result<(), ()> {
        match self.create_screencast_session(None, true).await {
            Ok(_) => Ok(()),
            Err(_) => Err(()),
        }
    }
}

impl ScreencastElement {
    async fn create_screencast_session(
        &self,
        window_identifier: Option<ashpd::WindowIdentifier>,
        restore_session: bool,
    ) -> Result<(), ashpd::Error> {
        let screencast = Screencast::new().await?;

        // TODO: store this
        let session = screencast.create_session().await?;

        let mut sources: BitFlags<SourceType> = BitFlags::empty();
        sources.insert(SourceType::Monitor);

        let restore_token = if restore_session {
            Some(String::from(
                SETTINGS.string("monitor-restore-token").as_str(),
            ))
            .filter(|s| !s.is_empty())
        } else {
            None::<String>
        };

        screencast
            .select_sources(
                &session,
                CursorMode::Embedded, // FIXME: switch to metadata
                sources,
                false,
                restore_token.as_deref(),
                PersistMode::ExplicitlyRevoked,
            )
            .await?;

        let response = screencast
            .start(&session, &window_identifier.unwrap_or_default())
            .await?
            .response()?;

        // Save the new restore token
        {
            let new_restore_token = response.restore_token().unwrap_or("");
            SETTINGS
                .set_string("monitor-restore-token", new_restore_token)
                .unwrap();
        }

        let fd = screencast.open_pipe_wire_remote(&session).await?;
        let node = response.streams()[0].pipe_wire_node_id();

        self.start_screencast(fd, node);

        Ok(())
    }

    fn start_screencast<F: AsRawFd>(&self, fd: F, node_id: u32) {
        let pipewire_element = gst::ElementFactory::make("pipewiresrc")
            .property("client-name", "Liveblast")
            .property("fd", fd.as_raw_fd())
            .property("path", &node_id.to_string())
            .build()
            .unwrap();
        let capsfilter = gst::ElementFactory::make("capsfilter")
            .property(
                "caps",
                [gst_video::VideoCapsBuilder::new()
                    .features(["memory:DMABuf"])
                    .build()]
                .into_iter()
                .collect::<gst::Caps>(),
            )
            .build()
            .unwrap();
        let appsink = self.stream_producer.appsink();

        self.stop();

        self.pipeline()
            .add_many(&[&pipewire_element, &capsfilter, appsink.upcast_ref()])
            .unwrap();

        gst::Element::link_many(&[&pipewire_element, &capsfilter, appsink.upcast_ref()]).unwrap();

        self.start();
    }
}

impl Default for ScreencastElement {
    fn default() -> Self {
        let appsink = gst_app::AppSink::builder()
            .name("screencast-appsink")
            .build();
        let stream_producer = gst_utils::StreamProducer::from(&appsink);
        let subpipeline = SubPipeline::new("Screencast");

        Self {
            stream_producer,
            subpipeline: Arc::new(subpipeline),
        }
    }
}
