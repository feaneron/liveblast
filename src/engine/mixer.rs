// encoder.rs
//
// Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

// # Audio Mixing Pipeline
//
// audiotestsrc -- audiomixer -- appsrc

use gst::prelude::*;
use gtk::glib;
use gtk::subclass::prelude::*;
use std::cell::RefCell;

use crate::engine::pluggable::*;
use crate::engine::subpipeline::*;

mod imp {
    use super::*;

    pub struct Mixer {
        pub(super) audiomixer: gst::Element,
        pub(super) producers: RefCell<ContentConsumptionList>,
        pub(super) stream_producer: gst_utils::StreamProducer,
        pub(super) subpipeline: SubPipeline,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Mixer {
        const NAME: &'static str = "LbMixer";
        type Type = super::Mixer;

        fn new() -> Self {
            let appsink = gst_app::AppSink::builder()
                .async_(true)
                .name("appsink")
                .build();
            let audiomixer = gst::ElementFactory::make("audiomixer")
                .property_from_str("start-time-selection", "first")
                .property("force-live", true)
                .build()
                .unwrap();
            let stream_producer = gst_utils::StreamProducer::from(&appsink);

            Self {
                audiomixer,
                producers: RefCell::new(ContentConsumptionList::default()),
                stream_producer,
                subpipeline: SubPipeline::new("Audio Mixer"),
            }
        }
    }

    impl ObjectImpl for Mixer {
        fn constructed(&self) {
            self.parent_constructed();

            let obj = self.obj();
            let audioconvert = gst::ElementFactory::make("audioconvert").build().unwrap();
            let capsfilter = gst::ElementFactory::make("capsfilter")
                .property(
                    "caps",
                    [gst_audio::AudioCapsBuilder::new()
                        .any_features()
                        .channels(2)
                        .build()]
                    .into_iter()
                    .collect::<gst::Caps>(),
                )
                .build()
                .unwrap();
            let appsink = self.stream_producer.appsink();

            obj.pipeline()
                .add_many(&[
                    &self.audiomixer,
                    &audioconvert,
                    &capsfilter,
                    appsink.upcast_ref(),
                ])
                .unwrap();

            gst::Element::link_many(&[
                &self.audiomixer,
                &audioconvert,
                &capsfilter,
                appsink.upcast_ref(),
            ])
            .unwrap();

            obj.start();
        }
    }
}

impl HasSubPipeline for Mixer {
    fn subpipeline(&self) -> &SubPipeline {
        &self.imp().subpipeline
    }
}

glib::wrapper! {
    pub struct Mixer(ObjectSubclass<imp::Mixer>);
}

impl ContentConsumer for Mixer {
    fn consume_content_from(&self, producer: impl ContentProducer) {
        let stream_producer = producer
            .stream_producer(ContentType::AUDIO)
            .expect("Mixer requires an audio producer");

        self.stop();

        let appsrc = gst_app::AppSrc::builder().is_live(true).build();
        let queue = gst::ElementFactory::make("queue").build().unwrap();

        let bin = match producer.id() {
            Some(id) => gst::Bin::with_name(id),
            None => gst::Bin::new(),
        };
        bin.add_many(&[appsrc.upcast_ref(), &queue]).unwrap();
        gst::Element::link_many(&[appsrc.upcast_ref(), &queue]).unwrap();

        let ghost_pad = gst::GhostPad::builder_with_target(&queue.static_pad("src").unwrap())
            .unwrap()
            .name("src")
            .build();
        bin.add_pad(&ghost_pad).unwrap();

        self.pipeline().add(&bin).unwrap();

        gst::Element::link_many(&[bin.upcast_ref(), &self.imp().audiomixer]).unwrap();

        let link = stream_producer.add_consumer(&appsrc).unwrap();
        self.imp().producers.borrow_mut().push(
            producer.id(),
            ContentType::AUDIO,
            stream_producer,
            link,
            None, // TODO
            &[appsrc.upcast::<gst::Element>(), queue],
        );

        self.start();
    }
}

impl ContentProducer for Mixer {
    fn id(&self) -> Option<&str> {
        None
    }

    fn stream_producer(&self, content_type: ContentType) -> Option<gst_utils::StreamProducer> {
        match content_type {
            ContentType::AUDIO => Some(self.imp().stream_producer.clone()),
            _ => None,
        }
    }
}

impl Default for Mixer {
    fn default() -> Self {
        glib::Object::new()
    }
}
