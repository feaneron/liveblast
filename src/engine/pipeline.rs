// pipeline.rs
//
// Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

// # Main data pipeline
//
// This is a static scene with hardcoded sources, canvas size,
//
//  <Compositor> -- <Encoder¹> -- <Transmitter²>

use gst::prelude::*;
use gtk::glib;
use gtk::subclass::prelude::*;
use std::cell::Cell;

use crate::engine::layout_manager::*;
use crate::engine::pluggable::*;
use crate::engine::subpipeline::*;
use crate::engine::*;

mod imp {
    use super::*;

    #[derive(Debug, glib::Properties)]
    #[properties(wrapper_type = super::Pipeline)]
    pub struct Pipeline {
        #[property(get)]
        pub(super) compositor: Compositor,
        #[property(get)]
        pub(super) encoder: Encoder,
        #[property(get)]
        pub(super) layout_manager: LayoutManager,
        #[property(get)]
        pub(super) mixer: Mixer,
        #[property(get)]
        pub(super) transmitter: Transmitter,
        pub(super) active_outputs: Cell<u32>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Pipeline {
        const NAME: &'static str = "LbPipeline";
        type Type = super::Pipeline;

        fn new() -> Self {
            let compositor = Compositor::default();

            Self {
                compositor: compositor.clone(),
                encoder: Encoder::default(),
                layout_manager: LayoutManager::new(compositor),
                mixer: Mixer::default(),
                transmitter: Transmitter::default(),
                active_outputs: Cell::new(0),
            }
        }
    }

    impl ObjectImpl for Pipeline {
        fn constructed(&self) {
            self.parent_constructed();

            self.compositor.share_gl_context_with(self.encoder.clone());
            self.encoder.consume_content_from(self.compositor.clone());

            self.transmitter.consume_content_from(self.mixer.clone());
            self.transmitter.consume_content_from(self.encoder.clone());
        }

        fn properties() -> &'static [glib::ParamSpec] {
            Self::derived_properties()
        }

        fn property(&self, id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            self.derived_property(id, pspec)
        }
    }
}

glib::wrapper! {
    pub struct Pipeline(ObjectSubclass<imp::Pipeline>);
}

impl Default for Pipeline {
    fn default() -> Self {
        glib::Object::new()
    }
}

impl Pipeline {
    pub fn start_streaming(&self) {
        if self.transmitter().active() {
            return;
        }

        let active_outputs = self.imp().active_outputs.get();

        if active_outputs == 0 {
            self.encoder().start();
        }
        self.transmitter().start();

        self.imp().active_outputs.set(active_outputs + 1);
    }

    pub fn stop_streaming(&self) {
        if !self.transmitter().active() {
            return;
        }

        let active_outputs = self.imp().active_outputs.get();

        if active_outputs == 1 {
            self.encoder().stop();
        }
        self.transmitter().stop();

        self.imp().active_outputs.set(active_outputs - 1);
    }

    pub fn add_stream_element<T: StreamElement + Clone>(&self, stream_element: T) {
        let content_type = stream_element.content_type();

        // Stream elements must never advertise encoded video, but must
        // always advertise *some* type of content
        assert!(!content_type.is_empty() && (content_type & ContentType::ENCODED_VIDEO).is_empty());

        if !(content_type & ContentType::RAW_VIDEO).is_empty() {
            self.compositor().share_gl_context_with(stream_element.clone());
            self.compositor().consume_content_from(stream_element.clone());
        }

        if !(content_type & ContentType::AUDIO).is_empty() {
            self.mixer().consume_content_from(stream_element);
        }

        self.imp().layout_manager.apply_layout();
    }
}
