// engine.rs
//
// Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::sync::LazyLock;

mod compositor;
pub use compositor::Compositor;

pub mod elements;

mod encoder;
pub use encoder::Encoder;

mod engine;
pub use engine::Engine;

mod layout_manager;
pub use layout_manager::LayoutManager;

mod mixer;
pub use mixer::Mixer;

mod permission;
pub use permission::{Permission, PermissionController};

mod pipeline;
pub use pipeline::Pipeline;

mod pluggable;

mod settings;
pub static SETTINGS: LazyLock<settings::Settings> = LazyLock::new(settings::Settings::default);

mod services;
pub use services::{StreamingService, DEFAULT_SERVICES};

mod stream_element;
pub use stream_element::StreamElement;

mod subpipeline;

mod transmitter;
pub use transmitter::{Transmitter, TransmitterSettings};
