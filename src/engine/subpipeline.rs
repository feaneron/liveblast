// subpipeline.rs
//
// Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

use gst::prelude::*;
use gtk::glib;
use gtk::subclass::prelude::*;

mod imp {
    use super::*;
    use std::cell::{OnceCell, RefCell};

    #[derive(glib::Properties)]
    #[properties(wrapper_type = super::SubPipeline)]
    pub struct SubPipeline {
        #[property(get)]
        pub(super) active: RefCell<bool>,
        #[property(get)]
        pub(super) pipeline: OnceCell<gst::Pipeline>,
        #[property(get, set, construct_only)]
        name: OnceCell<String>,

        pub(super) acquired_gl_context: RefCell<Option<gst::context::Context>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SubPipeline {
        const NAME: &'static str = "LbSubPipeline";
        type Type = super::SubPipeline;

        fn new() -> Self {
            Self {
                active: RefCell::from(false),
                pipeline: OnceCell::new(),
                name: OnceCell::new(),
                acquired_gl_context: RefCell::new(None),
            }
        }
    }

    impl ObjectImpl for SubPipeline {
        fn constructed(&self) {
            self.parent_constructed();

            let pipeline = gst::Pipeline::builder()
                .name(self.name.get().unwrap())
                .build();
            pipeline.set_start_time(gst::ClockTime::NONE);
            pipeline.set_base_time(gst::ClockTime::ZERO);
            pipeline.use_clock(Some(&gst::SystemClock::obtain()));

            self.pipeline.set(pipeline).unwrap();

            // Monitor the bus
            let self_ = self.obj().clone();
            let bus = self_.pipeline().bus().unwrap();
            bus.set_sync_handler(move |_, msg| {
                match msg.view() {
                    gst::MessageView::Error(err) => {
                        println!(
                            "[{:>30}] Error received from element {:?} {}",
                            self_.pipeline().name(),
                            err.src().map(|s| s.path_string()),
                            err.error()
                        );
                    }
                    gst::MessageView::StateChanged(state_change) => {
                        if state_change
                            .src()
                            .map(|s| s.downcast_ref::<gst::Pipeline>() == Some(&self_.pipeline()))
                            .unwrap_or(false)
                        {
                            println!(
                                "[{:>30}] State changed from {:?} to {:?}",
                                self_.pipeline().name(),
                                state_change.old(),
                                state_change.current()
                            );
                        }
                    }
                    gst::MessageView::HaveContext(have_context) => {
                        if have_context
                            .src()
                            .map(|s| s.downcast_ref::<gst::Pipeline>() == Some(&self_.pipeline()))
                            .unwrap_or(false)
                        {
                            let context = have_context.context();
                            let context_type = context.context_type();

                            println!(
                                "[{:>30}] Pipeline got context {:?}",
                                self_.pipeline().name(),
                                context_type
                            );

                            if context_type == *gst_gl::GL_DISPLAY_CONTEXT_TYPE {
                                self_.imp().acquired_gl_context.replace(Some(context));
                            }
                        }
                    }
                    _ => (),
                }

                gst::BusSyncReply::Pass
            });
        }

        fn properties() -> &'static [glib::ParamSpec] {
            Self::derived_properties()
        }

        fn set_property(&self, id: usize, value: &glib::Value, pspec: &glib::ParamSpec) {
            self.derived_set_property(id, value, pspec)
        }

        fn property(&self, id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            self.derived_property(id, pspec)
        }
    }

    impl std::fmt::Debug for SubPipeline {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            let obj = self.obj();

            let name = obj.name();
            f.debug_struct("SubPipeline")
                .field("active", &obj.active())
                .field("pipeline", &name)
                .finish()
        }
    }

    // FIXME: this is probably incorrect
    unsafe impl Send for SubPipeline {}
    unsafe impl Sync for SubPipeline {}
}

glib::wrapper! {
    pub struct SubPipeline(ObjectSubclass<imp::SubPipeline>);
}

pub trait HasSubPipeline: Clone {
    fn subpipeline(&self) -> &SubPipeline;
}

pub trait SubPipelineControls {
    fn active(&self) -> bool;
    fn pipeline(&self) -> gst::Pipeline;
    fn start(&self);
    fn stop(&self);
}

impl<O: HasSubPipeline> SubPipelineControls for O {
    fn active(&self) -> bool {
        self.subpipeline().active()
    }

    fn pipeline(&self) -> gst::Pipeline {
        self.subpipeline().pipeline()
    }

    fn start(&self) {
        if self.active() {
            return;
        }

        let subpipeline = self.subpipeline();

        match subpipeline.pipeline().set_state(gst::State::Playing) {
            Ok(_) => {
                subpipeline.imp().active.replace(true);
            }
            Err(error) => {
                println!(
                    "[{}] Error changing state: {}",
                    subpipeline.pipeline().name(),
                    error
                );
            }
        }
    }

    fn stop(&self) {
        if !self.active() {
            return;
        }

        let subpipeline = self.subpipeline();

        match subpipeline.pipeline().set_state(gst::State::Null) {
            Ok(_) => {
                subpipeline.imp().active.replace(false);
            }
            Err(error) => {
                println!(
                    "[${}] Error changing state: ${}",
                    subpipeline.pipeline().name(),
                    error
                );
            }
        }
    }
}

impl SubPipeline {
    pub fn new(name: &str) -> Self {
        glib::Object::builder().property("name", name).build()
    }

    pub(super) fn gl_context(&self) -> Option<gst::context::Context> {
        self.imp().acquired_gl_context.borrow().as_ref().cloned()
    }
}
