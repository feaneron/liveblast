// encoder.rs
//
// Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

// # Encoding Pipeline
//
// appsink -- queue -- vapostproc -- vah264enc -- h264parse -- appsrc
//

use gst::prelude::*;
use gtk::glib;
use gtk::subclass::prelude::*;
use std::cell::RefCell;

use crate::engine::pluggable::*;
use crate::engine::subpipeline::*;

mod imp {
    use super::*;

    pub struct Encoder {
        pub(super) appsrc: gst_app::AppSrc,
        pub(super) producers: RefCell<ContentConsumptionList>,
        pub(super) stream_producer: gst_utils::StreamProducer,
        pub(super) subpipeline: SubPipeline,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Encoder {
        const NAME: &'static str = "LbEncoder";
        type Type = super::Encoder;

        fn new() -> Self {
            let appsrc = gst_app::AppSrc::builder()
                .is_live(true)
                .name("appsrc")
                .build();
            let appsink = gst_app::AppSink::builder()
                .async_(true)
                .name("appsink")
                .build();
            let stream_producer = gst_utils::StreamProducer::from(&appsink);

            Self {
                appsrc,
                producers: RefCell::new(ContentConsumptionList::default()),
                stream_producer,
                subpipeline: SubPipeline::new("Encoder"),
            }
        }
    }

    impl ObjectImpl for Encoder {
        fn constructed(&self) {
            self.parent_constructed();

            let appsink = self.stream_producer.appsink();
            let queue = gst::ElementFactory::make("queue").build().unwrap();
            let glcolorconvert = gst::ElementFactory::make("glcolorconvert").build().unwrap();
            let gldownload = gst::ElementFactory::make("gldownload").build().unwrap();
            let capsfilter = gst::ElementFactory::make("capsfilter")
                .property(
                    "caps",
                    [
                        gst_video::VideoCapsBuilder::new()
                            .features(["memory:DMABuf"])
                            .format(gst_video::VideoFormat::Nv12)
                            .build(),
                        gst_video::VideoCapsBuilder::new()
                            .format(gst_video::VideoFormat::Nv12)
                            .build(),
                    ]
                    .into_iter()
                    .collect::<gst::Caps>(),
                )
                .build()
                .unwrap();

            let vapostproc = gst::ElementFactory::make("vapostproc").build().unwrap();
            let vah264enc = gst::ElementFactory::make("vah264enc")
                .property("aud", true)
                .property("cabac", true)
                .property("dct8x8", true)
                .property("bitrate", 5000u32)
                .property("key-int-max", 120u32)
                .property("target-usage", 3u32)
                .property("trellis", true)
                .build()
                .unwrap();

            self.obj().pipeline()
                .add_many(&[
                    self.appsrc.upcast_ref(),
                    &queue,
                    &glcolorconvert,
                    &gldownload,
                    &capsfilter,
                    &vapostproc,
                    &vah264enc,
                    appsink.upcast_ref(),
                ])
                .unwrap();

            gst::Element::link_many(&[
                self.appsrc.upcast_ref(),
                &queue,
                &glcolorconvert,
                &gldownload,
                &capsfilter,
                &vapostproc,
                &vah264enc,
                appsink.upcast_ref(),
            ])
            .unwrap();
        }
    }
}

impl HasSubPipeline for Encoder {
    fn subpipeline(&self) -> &SubPipeline {
        &self.imp().subpipeline
    }
}

glib::wrapper! {
    pub struct Encoder(ObjectSubclass<imp::Encoder>);
}

impl ContentConsumer for Encoder {
    fn consume_content_from(&self, producer: impl ContentProducer) {
        let stream_producer = producer.stream_producer(ContentType::RAW_VIDEO)
            .expect("Encoder requires a raw video producer");

        let link = stream_producer.add_consumer(&self.imp().appsrc).unwrap();
        self.imp().producers.borrow_mut().push(
            producer.id(),
            ContentType::RAW_VIDEO,
            stream_producer,
            link,
            None,
            &[]
        );
    }
}

impl ContentProducer for Encoder {
    fn id(&self) -> Option<&str> {
        None
    }

    fn stream_producer(&self, content_type: ContentType) -> Option<gst_utils::StreamProducer> {
        match content_type {
            ContentType::ENCODED_VIDEO => Some(self.imp().stream_producer.clone()),
            _ => None,
        }
    }
}

impl Default for Encoder {
    fn default() -> Self {
        glib::Object::new()
    }
}
