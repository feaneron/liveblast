// services.rs
//
// Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

#[derive(Clone, Debug)]
pub struct StreamingService {
    pub id: &'static str,
    pub name: &'static str,
    pub endpoint: &'static str,
}

pub const DEFAULT_SERVICES: [StreamingService; 2] = [
    StreamingService {
        id: "twitch",
        name: "Twitch",
        endpoint: "rtmp://live.twitch.tv/app/",
    },
    StreamingService {
        id: "youtube",
        name: "YouTube",
        endpoint: "rtmps://a.rtmps.youtube.com:443/live2/",
    },
];

impl StreamingService {
    pub fn format_location_url(&self, stream_key: &str) -> String {
        format!("{}{stream_key}", self.endpoint)
    }
}
