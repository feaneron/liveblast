// welcome.rs
//
// Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

use adw::prelude::*;
use adw::subclass::prelude::*;
use gtk::glib;
use std::cell::RefCell;

use crate::application::Application;
use crate::engine::*;
use crate::widgets::{PermissionState, PermissionStateWidget};

mod imp {
    use super::*;

    #[derive(Debug, Default, gtk::CompositeTemplate)]
    #[template(resource = "/com/feaneron/Liveblast/welcome.ui")]
    pub struct WelcomeDialog {
        #[template_child]
        pub(super) cameras_permission_state_widget: TemplateChild<PermissionStateWidget>,
        #[template_child]
        pub(super) microphone_permission_state_widget: TemplateChild<PermissionStateWidget>,
        #[template_child]
        pub(super) monitor_permission_state_widget: TemplateChild<PermissionStateWidget>,
        #[template_child]
        pub(super) stack: TemplateChild<gtk::Stack>,
        pub(super) n_permissions: RefCell<u32>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for WelcomeDialog {
        const NAME: &'static str = "LbWelcomeDialog";
        type Type = super::WelcomeDialog;
        type ParentType = adw::Window;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();

            klass.install_action(
                "welcome-dialog.next",
                None,
                move |dialog, _, _| match dialog.imp().stack.visible_child_name().unwrap().as_str()
                {
                    "welcome" => dialog.imp().stack.set_visible_child_name("permissions"),
                    "permissions" => dialog.imp().stack.set_visible_child_name("end"),
                    _ => (),
                },
            );

            klass.install_action_async(
                "welcome-dialog.request-camera",
                None,
                move |dialog, _, _| async move {
                    match dialog.request_camera().await {
                        Ok(_) => log::debug!("Finished"),
                        Err(err) => log::error!("Failed to request camera access: {err}"),
                    }
                },
            );

            klass.install_action_async(
                "welcome-dialog.request-microphone",
                None,
                move |dialog, _, _| async move {
                    match dialog.request_microphone().await {
                        Ok(_) => log::debug!("Finished"),
                        Err(err) => log::error!("Failed to request microphone access: {err}"),
                    }
                },
            );

            klass.install_action_async(
                "welcome-dialog.request-monitor",
                None,
                move |dialog, _, _| async move {
                    match dialog.request_monitor().await {
                        Ok(_) => log::debug!("Finished"),
                        Err(err) => log::error!("Failed to request monitor access: {err}"),
                    }
                },
            );

            klass.install_action("welcome-dialog.finish", None, move |dialog, _, _| {
                SETTINGS.set_boolean("first-run", false).unwrap();
                dialog.close();
            });
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for WelcomeDialog {}
    impl WidgetImpl for WelcomeDialog {}

    impl WindowImpl for WelcomeDialog {
        fn close_request(&self) -> glib::signal::Propagation {
            let n_permissions = *self.n_permissions.borrow();
            if n_permissions < 3 {
                Application::default_instance().quit();
                return glib::signal::Propagation::Stop;
            }
            glib::signal::Propagation::Proceed
        }
    }

    impl AdwWindowImpl for WelcomeDialog {}
}

glib::wrapper! {
    pub struct WelcomeDialog(ObjectSubclass<imp::WelcomeDialog>)
        @extends gtk::Widget, gtk::Window, adw::Window;
}

impl WelcomeDialog {
    async fn request_camera(&self) -> ashpd::Result<()> {
        self.imp()
            .cameras_permission_state_widget
            .set_permission_state(PermissionState::Requesting);

        let camera = Application::default_instance().engine().camera();

        match camera.request_permission(None).await? {
            Permission::Granted => {
                self.action_set_enabled("welcome-dialog.request-camera", false);
                self.imp()
                    .cameras_permission_state_widget
                    .set_permission_state(PermissionState::Granted);
                self.push_and_check_permissions();
                Ok(())
            }
            _ => {
                self.action_set_enabled("welcome-dialog.request-camera", true);
                self.imp()
                    .cameras_permission_state_widget
                    .set_permission_state(PermissionState::Pending);
                Ok(())
            }
        }
    }

    async fn request_microphone(&self) -> ashpd::Result<()> {
        self.imp()
            .microphone_permission_state_widget
            .set_permission_state(PermissionState::Requesting);

        let microphone = Application::default_instance().engine().microphone();

        let root = self.native().unwrap();
        let identifier = ashpd::WindowIdentifier::from_native(&root).await;

        match microphone.request_permission(Some(identifier)).await? {
            Permission::Granted => {
                self.action_set_enabled("welcome-dialog.request-microphone", false);
                self.imp()
                    .microphone_permission_state_widget
                    .set_permission_state(PermissionState::Granted);
                self.push_and_check_permissions();
                Ok(())
            }
            _ => {
                self.action_set_enabled("welcome-dialog.request-microphone", true);
                self.imp()
                    .microphone_permission_state_widget
                    .set_permission_state(PermissionState::Pending);
                Ok(())
            }
        }
    }

    async fn request_monitor(&self) -> ashpd::Result<()> {
        self.imp()
            .monitor_permission_state_widget
            .set_permission_state(PermissionState::Requesting);

        let screencast = Application::default_instance().engine().screencast();

        let root = self.native().unwrap();
        let identifier = ashpd::WindowIdentifier::from_native(&root).await;

        match screencast.request_permission(Some(identifier)).await? {
            Permission::Granted => {
                self.action_set_enabled("welcome-dialog.request-monitor", false);
                self.imp()
                    .monitor_permission_state_widget
                    .set_permission_state(PermissionState::Granted);
                self.push_and_check_permissions();
                Ok(())
            }
            _ => {
                self.action_set_enabled("welcome-dialog.request-monitor", true);
                self.imp()
                    .monitor_permission_state_widget
                    .set_permission_state(PermissionState::Pending);
                Ok(())
            }
        }
    }

    fn push_and_check_permissions(&self) {
        let mut n_permissions = self.imp().n_permissions.borrow_mut();
        *n_permissions += 1;

        if *n_permissions == 3 {
            self.activate_action("welcome-dialog.next", None)
                .expect("Failed to activate 'welcome-dialog.next' action");
        }

        drop(n_permissions);
    }
}

impl Default for WelcomeDialog {
    fn default() -> Self {
        glib::Object::new()
    }
}
