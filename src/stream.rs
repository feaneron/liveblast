// stream_dialog.rs
//
// Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

use adw::prelude::*;
use adw::subclass::prelude::*;
use gtk::glib;

use crate::application::Application;
use crate::engine::*;

mod imp {
    use super::*;

    #[derive(Debug, Default, gtk::CompositeTemplate)]
    #[template(resource = "/com/feaneron/Liveblast/stream.ui")]
    pub struct StreamDialog {
        #[template_child]
        services_combo_row: TemplateChild<adw::ComboRow>,
        #[template_child]
        services_model: TemplateChild<gtk::StringList>,
        #[template_child]
        stream_key_row: TemplateChild<adw::PasswordEntryRow>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for StreamDialog {
        const NAME: &'static str = "LbStreamDialog";
        type Type = super::StreamDialog;
        type ParentType = adw::Window;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();

            klass.install_action("stream-dialog.go-live", None, move |dialog, _, _| {
                dialog.imp().go_live();
            });
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for StreamDialog {
        fn constructed(&self) {
            self.parent_constructed();

            for service in &DEFAULT_SERVICES {
                self.services_model.append(service.name);
            }

            SETTINGS
                .bind("service", &*self.services_combo_row, "selected")
                .get()
                .mapping(|v, _| {
                    Some(
                        DEFAULT_SERVICES
                            .iter()
                            .position(|s| s.id == v.str().unwrap())
                            .map(|r| u32::try_from(r).unwrap())
                            .map(|r| r.to_value())
                            .unwrap_or_else(|| gtk::INVALID_LIST_POSITION.to_value()),
                    )
                })
                .build();

            SETTINGS
                .bind("stream-key", &*self.stream_key_row, "text")
                .get()
                .build();
        }
    }

    impl WidgetImpl for StreamDialog {}
    impl WindowImpl for StreamDialog {}
    impl AdwWindowImpl for StreamDialog {}
    impl AdwDialogImpl for StreamDialog {}
    impl PreferencesDialogImpl for StreamDialog {}

    impl StreamDialog {
        fn go_live(&self) {
            let selected_service = self.services_combo_row.selected();
            let service = match selected_service {
                gtk::INVALID_LIST_POSITION => None,
                _ => Some(DEFAULT_SERVICES[selected_service as usize].clone()),
            };

            let pipeline = Application::default_instance().engine().pipeline();

            pipeline.transmitter().apply_settings(TransmitterSettings {
                service,
                stream_key: self.stream_key_row.text().to_string(),
            });

            pipeline.start_streaming();

            self.obj().close();
        }
    }
}

glib::wrapper! {
    pub struct StreamDialog(ObjectSubclass<imp::StreamDialog>)
        @extends gtk::Widget, gtk::Window, adw::Window, adw::PreferencesDialog;
}

impl Default for StreamDialog {
    fn default() -> Self {
        glib::Object::new()
    }
}

impl StreamDialog {}
