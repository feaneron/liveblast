// permission_state.rs
//
// Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::glib;

mod imp {
    use super::*;

    #[derive(Debug, Default)]
    pub struct PermissionStateWidget {
        pub(super) stack: gtk::Stack,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for PermissionStateWidget {
        const NAME: &'static str = "LbPermissionStateWidget";
        type Type = super::PermissionStateWidget;
        type ParentType = gtk::Widget;

        fn class_init(klass: &mut Self::Class) {
            klass.set_layout_manager_type::<gtk::BinLayout>();
        }
    }

    impl ObjectImpl for PermissionStateWidget {
        fn constructed(&self) {
            self.parent_constructed();

            let obj = self.obj();
            self.stack.set_parent(&*obj);

            let not_okay_icon = gtk::Image::from_icon_name("process-stop-symbolic");
            not_okay_icon.add_css_class("error");
            self.stack.add_named(not_okay_icon.upcast_ref::<gtk::Widget>(), Some("not-okay"));

            let requesting_spinner = gtk::Spinner::new();
            requesting_spinner.set_spinning(true);
            self.stack.add_named(requesting_spinner.upcast_ref::<gtk::Widget>(), Some("requesting"));

            let okay_icon = gtk::Image::from_icon_name("object-select-symbolic");
            okay_icon.add_css_class("success");
            self.stack.add_named(okay_icon.upcast_ref::<gtk::Widget>(), Some("okay"));
        }

        fn dispose(&self) {
            self.stack.unparent();
        }
    }

    impl WidgetImpl for PermissionStateWidget {}
}

glib::wrapper! {
    pub struct PermissionStateWidget(ObjectSubclass<imp::PermissionStateWidget>)
        @extends gtk::Widget;
}

impl Default for PermissionStateWidget {
    fn default() -> Self {
        glib::Object::new()
    }
}

pub enum PermissionState {
    Pending,
    Requesting,
    Granted,
}

impl PermissionStateWidget {
    pub fn new() -> Self {
        PermissionStateWidget::default()
    }

    pub fn set_permission_state(&self, state: PermissionState) {
        match state {
            PermissionState::Pending => self.imp().stack.set_visible_child_name("not-okay"),
            PermissionState::Requesting => self.imp().stack.set_visible_child_name("requesting"),
            PermissionState::Granted => self.imp().stack.set_visible_child_name("okay"),
        }
    }
}
