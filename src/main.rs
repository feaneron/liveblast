/* main.rs
 *
 * Copyright 2023 Georges Stavracas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

mod application;
#[rustfmt::skip]
mod config;
mod engine;
mod preferences;
mod stream;
mod welcome;
mod widgets;
mod window;

use self::application::Application;

use gettextrs::{bind_textdomain_codeset, bindtextdomain, textdomain};
use gtk::{gio, glib};
use gtk::prelude::*;
use log::debug;

fn main() -> glib::ExitCode {

    debug!("Binding gettext domain");
    bindtextdomain(config::GETTEXT_PACKAGE, config::LOCALEDIR)
        .expect("Unable to bind the text domain");
    bind_textdomain_codeset(config::GETTEXT_PACKAGE, "UTF-8")
        .expect("Unable to set the text domain encoding");
    textdomain(config::GETTEXT_PACKAGE)
        .expect("Unable to switch to the text domain");

    debug!("Loading resources");
    let resources = gio::Resource::load(config::PKGDATADIR.to_owned() + "/liveblast.gresource")
        .expect("Could not load resources");
    gio::resources_register(&resources);

    glib::set_application_name("Liveblast");

    let app = Application::default();

    app.run()
}
