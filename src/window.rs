/* window.rs
 *
 * Copyright 2023 Georges Stavracas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

use adw::prelude::*;
use adw::subclass::prelude::*;
use gtk::{gio, glib};

use crate::engine;
use crate::preferences::PreferencesDialog;
use crate::welcome::WelcomeDialog;

mod imp {
    use super::*;

    #[derive(Debug, Default, gtk::CompositeTemplate)]
    #[template(resource = "/com/feaneron/Liveblast/window.ui")]
    pub struct Window {
        // Template widgets
        #[template_child]
        pub header_bar: TemplateChild<gtk::HeaderBar>,
        #[template_child]
        pub picture: TemplateChild<gtk::Picture>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Window {
        const NAME: &'static str = "LbWindow";
        type Type = super::Window;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();

            // TODO: disable while streaming
            klass.install_action("window.show-preferences", None, move |window, _, _| {
                let preferences_dialog = PreferencesDialog::default();
                preferences_dialog.present(Some(window));
            });
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for Window {}

    impl WidgetImpl for Window {
        fn map(&self) {
            self.parent_map();

            if engine::SETTINGS.boolean("first-run") {
                let welcome_dialog = WelcomeDialog::default();
                welcome_dialog.set_transient_for(Some(self.obj().as_ref()));
                welcome_dialog.present();
            } else {
                let engine = crate::Application::default_instance().engine();
                // TODO: check permissions
                engine.start_stream_elements();
            }
        }
    }

    impl WindowImpl for Window {}
    impl ApplicationWindowImpl for Window {}
    impl AdwApplicationWindowImpl for Window {}

    #[gtk::template_callbacks]
    impl Window {
        #[template_callback]
        fn on_stream_toggle_button_toggled_cb(&self, button: &gtk::ToggleButton) {
            let application = crate::Application::default_instance();
            let pipeline = application.engine().pipeline();

            if button.is_active() {
                application.activate_action("stream", None);
            } else {
                pipeline.stop_streaming();
            }
        }
    }
}

glib::wrapper! {
    pub struct Window(ObjectSubclass<imp::Window>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, adw::ApplicationWindow,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl Window {
    pub fn new<P: IsA<gtk::Application>>(application: &P) -> Self {
        let win = glib::Object::builder::<Window>()
            .property("application", application)
            .build();

        win.setup_pipeline_preview();

        win
    }

    fn setup_pipeline_preview(&self) {
        let paintable = crate::Application::default_instance()
            .engine()
            .pipeline()
            .compositor()
            .get_preview();

        self.imp().picture.set_paintable(paintable.as_ref());
    }
}
