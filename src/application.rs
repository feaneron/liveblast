/* application.rs
 *
 * Copyright 2023 Georges Stavracas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

use adw::prelude::*;
use adw::subclass::prelude::*;
use gtk::{gio, glib};

use crate::config::*;
use crate::engine::Engine;
use crate::stream::StreamDialog;
use crate::window::Window;

mod imp {
    use super::*;
    use std::cell::OnceCell;
    use std::sync::LazyLock;

    pub struct Application {
        pub engine: OnceCell<Engine>,
        sentry_guard: Option<sentry::ClientInitGuard>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Application {
        const NAME: &'static str = "LbApplication";
        type Type = super::Application;
        type ParentType = adw::Application;

        fn new() -> Self {
            let sentry_guard = Some(SENTRY_ENDPOINT)
                .filter(|s| !s.is_empty())
                .map(|endpoint| {
                    sentry::init((
                        endpoint,
                        sentry::ClientOptions {
                            release: sentry::release_name!(),
                            ..Default::default()
                        },
                    ))
                });

            Self {
                engine: OnceCell::new(),
                sentry_guard,
            }
        }
    }

    impl ObjectImpl for Application {
        fn constructed(&self) {
            self.parent_constructed();

            self.obj().setup_gactions();
        }

        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: LazyLock<Vec<glib::ParamSpec>> = LazyLock::new(|| {
                vec![glib::ParamSpecObject::builder::<Engine>("engine")
                    .read_only()
                    .build()]
            });
            PROPERTIES.as_ref()
        }

        fn property(&self, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            match pspec.name() {
                "engine" => self.obj().engine().to_value(),
                _ => unreachable!(),
            }
        }
    }

    impl ApplicationImpl for Application {
        fn startup(&self) {
            self.parent_startup();

            let sentry_guard = &self.sentry_guard;
            println!(
                "Sentry integration: {}",
                sentry_guard
                    .as_ref()
                    .filter(|s| s.is_enabled())
                    .map_or("disabled", |_| "enabled")
            );

            let style_manager = adw::StyleManager::default();
            style_manager.set_color_scheme(adw::ColorScheme::ForceDark);

            let engine = Engine::default();
            self.engine.set(engine).expect("Failed to set engine");
        }

        fn activate(&self) {
            let application = self.obj();

            let window = if let Some(window) = application.active_window() {
                window
            } else {
                Window::new(&*application).upcast()
            };

            window.present();
        }
    }

    impl GtkApplicationImpl for Application {}
    impl AdwApplicationImpl for Application {}
}

glib::wrapper! {
    pub struct Application(ObjectSubclass<imp::Application>)
        @extends gio::Application, gtk::Application, adw::Application,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl Default for Application {
    fn default() -> Self {
        glib::Object::builder()
            .property("application-id", "com.feaneron.Liveblast")
            .build()
    }
}

impl Application {
    pub fn default_instance() -> Self {
        gio::Application::default()
            .and_downcast()
            .expect("Default application instance not found")
    }

    pub fn engine(&self) -> Engine {
        self.imp()
            .engine
            .get()
            .expect("Engine not available")
            .clone()
    }

    fn setup_gactions(&self) {
        self.add_action_entries([
            gio::ActionEntry::builder("quit")
                .activate(move |app: &Self, _, _| app.quit())
                .build(),
            gio::ActionEntry::builder("about")
                .activate(move |app: &Self, _, _| app.show_about())
                .build(),
            gio::ActionEntry::builder("stream")
                .activate(move |app: &Self, _, _| app.stream())
                .build(),
        ]);
        self.set_accels_for_action("app.quit", &["<primary>q"]);
    }

    fn show_about(&self) {
        let window = self.active_window().unwrap();
        adw::AboutDialog::builder()
            .application_name("Liveblast")
            .application_icon("com.feaneron.Liveblast")
            .developer_name("Georges Basile Stavracas Neto")
            .version(VERSION)
            .developers(vec!["Georges Basile Stavracas Neto"])
            .designers(vec!["Georges Basile Stavracas Neto", "Jakub Steiner"])
            .copyright("© 2024 Georges Stavracas")
            .build()
            .present(Some(&window));
    }

    fn stream(&self) {
        let window = self.active_window();
        let stream_dialog = StreamDialog::default();
        stream_dialog.set_transient_for(window.as_ref());
        stream_dialog.present();
    }
}
